import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Header from '../components/Header';
import HomePage from "../components/HomePage";
import Portfolio from '../components/Portfolio';
import ContactPage from "../components/ContactPage";
import NotFoundPage from '../components/NotFoundPage';
import PortfolioPage from "../components/PortfolioPage";

const AppRouter = () => (
  <BrowserRouter>
    <div>
      <Header />
      <Switch>
        <Route exact={true} path="/" component={HomePage} />
        <Route exact={true} path="/portfolio" component={PortfolioPage} />
        <Route path="/portfolio/:id" component={Portfolio} />
        <Route path="/contact" component={ContactPage} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </BrowserRouter>
);

export default AppRouter;