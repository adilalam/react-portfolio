import React from "react";

const ContactPage = () => (
  <div>
    <h1>Contact Me</h1>
    <p>This is Contact Page. You Can reach me at xyz.com</p>
  </div>
);

export default ContactPage;